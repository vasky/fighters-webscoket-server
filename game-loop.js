const { checkCollisionAttack } = require("./utils");
const { CANVAS_HEIGHT } = require("./constants");

const gameLoop = (state, numberPlayer) => {
  const player = state["players"][numberPlayer - 1];
  const player2 = state.players.find((playerItem) => playerItem !== player);

  if (player.attacking && !player.causedDamageInThisHit) {
    checkCollisionAttack(player, player2, () => {
      player.causedDamageInThisHit = true;
      player2.health -= player.damage;
    });
  }

  const topYStayGround = CANVAS_HEIGHT - player.height;

  if (player.pressedButtons.w && player.y === topYStayGround) {
    player.velocityY -= 40;
  }

  player.x += player.velocityX;
  player.y += player.velocityY;

  if (player.health <= 0) {
    player.playerDied = true;
  }

  if (player.velocityY < 0 || player.y < topYStayGround) {
    player.velocityY += player.gravityPower;
  } else {
    player.velocityY = 0;
    player.y = topYStayGround;
  }

  if (player.pressedButtons.d) {
    player.velocityX = player.playerSpeed;
  } else if (player.pressedButtons.a) {
    player.velocityX = -player.playerSpeed;
  } else {
    player.velocityX = 0;
  }

  if (player.pressedButtons.space && !player.attacking) {
    player.attacking = true;
  }

  if (player.pressedButtons.s && player.y < topYStayGround) {
    player.velocityY += 10;
  }
};

module.exports = {
  gameLoop,
};
