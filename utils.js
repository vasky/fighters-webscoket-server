const { ATTACK_COLLIDER_BOX_WIDTH } = require("./constants");

const handleKeyDownUp = (key, playerState, nextValue) => {
  switch (key.data) {
    case "KeyW":
      if (nextValue) playerState.lastPressedButton = "KeyW";
      playerState.pressedButtons.w = nextValue;
      break;
    case "KeyS":
      if (nextValue) playerState.lastPressedButton = "KeyS";
      playerState.pressedButtons.s = nextValue;
      break;
    case "KeyA":
      if (nextValue) playerState.lastPressedButton = "KeyA";
      playerState.pressedButtons.a = nextValue;
      break;
    case "KeyD":
      if (nextValue) playerState.lastPressedButton = "KeyD";
      playerState.pressedButtons.d = nextValue;
      break;
    case "Space":
      if (nextValue) playerState.lastPressedButton = "Space";
      playerState.pressedButtons.space = nextValue;
      break;
  }
};

const checkCollisionAttack = (
  playerAttacking,
  playerBeaten,
  onSuccessAttack
) => {
  if (!playerAttacking || !playerBeaten) {
    return;
  }

  const xPositionAttackBoxColliderRight =
    playerAttacking.x + playerAttacking.width + ATTACK_COLLIDER_BOX_WIDTH;

  if (
    (playerAttacking.pressedButtons.a &&
      playerAttacking.x - ATTACK_COLLIDER_BOX_WIDTH <=
        playerBeaten.x + playerBeaten.width &&
      playerAttacking.x >= playerBeaten.x) ||
    (playerAttacking.x + playerAttacking.width <=
      playerBeaten.x + playerBeaten.width &&
      xPositionAttackBoxColliderRight >= playerBeaten.x &&
      playerAttacking.y <= playerBeaten.y + playerBeaten.height &&
      playerAttacking.height + playerAttacking.y >= playerBeaten.y)
  ) {
    onSuccessAttack();
  }
};

const handleFinishAttack = (playerAttacking) => {
  playerAttacking.attacking = false;
  playerAttacking.causedDamageInThisHit = false;
};

module.exports = { handleKeyDownUp, checkCollisionAttack, handleFinishAttack };
