const { handleFinishAttack } = require("./utils");
const { FRAME_RATE } = require("./constants");
const { createGameState } = require("./game-state");
const server = require("https").createServer();
const { handleKeyDownUp } = require("./utils");
const { gameLoop } = require("./game-loop");
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },

  transports: ["websocket", "polling", "flashsocket"],
});

const state = {};

io.on("connection", (socket) => {
  socket.on("connect_error", (err) => {
    console.log(`connect_error due to ${err.message}`);
  });
  socket.on("init", (data, cb) => {
    const room = io.sockets.adapter.rooms.get(data?.roomName);

    // Получеть колиство человек в комнате, отправлять пользователю его номер(1 либо 2)
    // Номер нужен для того что бы в дальнейшем понимать, какой игрок, и изменять нужные параметры(pressedButtons, healths)
    let numClients = room?.size;

    if (numClients >= 2) {
      return cb?.("Больше двух игроков");
    }
    if (!numClients) {
      socket.number = 1;
      state[data?.roomName] = createGameState();
    } else if (numClients === 1) {
      socket.number = 2;
    }
    //
    socket.join?.(data?.roomName);
    socket.emit("successSignInRoom", { playerNumber: numClients });

    const activePlayer = state[data?.roomName]["players"][socket.number - 1];

    socket.on("keyDown", (data) => handleKeyDownUp(data, activePlayer, true));
    socket.on("keyUp", (data) => {
      handleKeyDownUp(data, activePlayer, false);
    });
    socket.on("finishAttack", () => {
      handleFinishAttack(state[data?.roomName].players[socket.number - 1]);
    });
    startGameInterval(socket, state[data?.roomName], socket.number);
  });
});
server.listen(3001);

console.log("success start");

const startGameInterval = (socket, gameState, numberPlayer) => {
  setInterval(() => {
    gameLoop(gameState, numberPlayer);
    socket.emit("updatePlayerState", gameState);
  }, 1000 / FRAME_RATE);
};
