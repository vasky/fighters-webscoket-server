const { CANVAS_HEIGHT } = require("./constants");
const createGameState = () => {
  return {
    players: [
      {
        height: 150,
        width: 75,
        x: 0,
        y: CANVAS_HEIGHT - 150,
        velocityX: 0,
        velocityY: 0,
        health: 100,
        pressedButtons: {
          a: false,
          w: false,
          s: false,
          d: false,
          space: false,
        },
        playerSpeed: 15,
        gravityPower: 2.5,
        damage: 10,
        causedDamageInThisHit: false,
      },
      {
        height: 150,
        width: 75,
        x: 0,
        y: CANVAS_HEIGHT - 150,
        velocityX: 0,
        velocityY: 0,
        health: 100,
        pressedButtons: {
          a: false,
          w: false,
          s: false,
          d: false,
          space: false,
        },
        playerSpeed: 15,
        gravityPower: 2.5,
        damage: 10,
        causedDamageInThisHit: false,
      },
    ],
  };
};

module.exports = {
  createGameState,
};
